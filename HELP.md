# MyDemo Spring Web Application

Make sure you have Docker, Java 8 or greater with Gradle plugin installed on your machine.

# Getting Started
**Step 1** \
clone this repository
```
git clone https://gitlab.com/alexeyrmnv/demo.git
```
**Step 2** \
build docker image
```
docker build -f Dockerfile -t mydemo .
```
**step 3** \
run the application in the docker
```
docker run -p 8080:8080 mydemo
```
# Usage
To get information about the application’s environment in json format open the link: http://localhost:8080/env

You could also use CURL to get the status code and addition information e.g. 
```
curl http://localhost:8080/env -i -X GET 
```
