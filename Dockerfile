# Docker Image
FROM openjdk:8-jdk-alpine
# Make user and group spring to run the app as a non-root user
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
# Compile my project
ADD build/libs/*.jar mydemo.jar
# Port mapping
EXPOSE 8080
# Run
ENTRYPOINT ["java", "-jar", "mydemo.jar"]